package com.example.teamweatherapp;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class WeatherInfoManager
{
    private RequestQueue requestQueue;
    private JSONObject weatherInfo;
    private final String URL = "http://weather.livedoor.com/forecast/webservice/json/v1?city=130010";

    private static final WeatherInfoManager ourInstance = new WeatherInfoManager();

    public static WeatherInfoManager getInstance() {
        return ourInstance;
    }

    private WeatherInfoManager() {
    }

    public JSONObject getWeatherInfo(final Context context) {
        if(weatherInfo == null) {
            if(requestQueue == null) {
                requestQueue = Volley.newRequestQueue(context);
            }

            // Write your code here

        }

        return weatherInfo;
    }


}
